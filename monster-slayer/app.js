new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns: []
  },
  methods: {
    startGame: function() {
      ;(this.gameIsRunning = true),
        (this.playerHealth = 100),
        (this.monsterHealth = 100),
        (this.turns = [])
    },
    attack: function() {
      this.playerAttack(10, 3)

      if (this.gameOver()) {
        return
      }

      this.monsterAttack(12, 5)

      this.gameOver()
    },
    specialAttack: function() {
      this.playerAttack(20, 10)

      if (this.gameOver()) {
        return
      }

      this.monsterAttack(12, 5)

      this.gameOver()
    },
    heal: function() {
      this.logger({ isPlayer: true, text: `Player heals for 10` })

      this.monsterAttack(12, 5)

      this.playerHealth =
        this.playerHealth + 10 <= 100
          ? this.playerHealth + 10
          : (this.playerHealth = 100)
    },
    giveUp: function() {
      this.gameIsRunning = false
    },
    monsterAttack(max, min) {
      var damage = this.damage(max, min)
      this.playerHealth -= damage
      this.playerHealth = this.playerHealth < 0 ? 0 : this.playerHealth
      this.logger({
        isPlayer: false,
        text: `Monster hits Player for ${damage}`
      })
    },
    playerAttack(max, min) {
      var damage = this.damage(max, min)
      var hard = max > 10 ? 'hard ' : ''
      this.monsterHealth -= damage
      this.monsterHealth = this.monsterHealth < 0 ? 0 : this.monsterHealth
      this.logger({
        isPlayer: true,
        text: `Player hits ${hard}Monster for ${damage}`
      })
    },
    damage: function(max, min) {
      return Math.max(Math.floor(Math.random() * max) + 1, min)
    },
    gameOver: function() {
      if (this.monsterHealth <= 0) {
        if (confirm('You won!!! New Game?')) {
          this.startGame()
        } else {
          this.gameIsRunning = false
        }
        return true
      } else if (this.playerHealth <= 0) {
        if (confirm('You lost!!! New Game?')) {
          this.startGame()
        } else {
          this.gameIsRunning = false
        }
        return true
      }
      return false
    },
    logger: function(obj) {
      this.turns.unshift(obj)
    }
  }
})
